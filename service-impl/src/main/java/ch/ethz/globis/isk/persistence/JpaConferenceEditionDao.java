package ch.ethz.globis.isk.persistence;

import ch.ethz.globis.isk.domain.Conference;
import ch.ethz.globis.isk.domain.ConferenceEdition;
import ch.ethz.globis.isk.domain.jpa.JpaConference;
import ch.ethz.globis.isk.domain.jpa.JpaConferenceEdition;

import org.springframework.stereotype.Repository;

import com.db4o.query.Query;

import java.util.List;

@Repository
public class JpaConferenceEditionDao extends JpaDao<String, ConferenceEdition> implements ConferenceEditionDao {

    @Override
    protected Class<JpaConferenceEdition> getStoredClass() {
        return JpaConferenceEdition.class;
    }

    @Override
    public ConferenceEdition createEntity() {
        return new JpaConferenceEdition();
    }

    @Override
    public List<ConferenceEdition> findByConferenceOrderedByYear(String conferenceId) {
    	//Conference proto = new JpaConference();
    	//proto.setId(conferenceId);
    	Query query = oc.query();
    	query.constrain(getStoredClass());
    	query.descend("conference").descend("id").constrain(conferenceId);
    	query.descend("year").orderAscending();
    	return query.execute();
    	  
    	  
//        String findAuthorsQuery = "Select ce from ConferenceEdition ce JOIN ce.conference c " +
//                "WHERE c.id = :conferenceId ORDER BY ce.year ASC";
//        Query query = em.createQuery(findAuthorsQuery);
//        query.setParameter("conferenceId", conferenceId);
//        return query.getResultList();
    }
    
}