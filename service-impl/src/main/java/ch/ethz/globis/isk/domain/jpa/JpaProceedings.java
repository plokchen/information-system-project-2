package ch.ethz.globis.isk.domain.jpa;


import ch.ethz.globis.isk.domain.*;

import java.util.HashSet;
import java.util.Set;


public class JpaProceedings extends JpaPublication implements Proceedings {
	private static Integer idCounter = 0;
	
    private String note;
    private Integer number;
    private String volume;
    private String isbn;

    private Publisher publisher;

    private Series series;

    private ConferenceEdition conferenceEdition;

    private Set<InProceedings> publications;

    public JpaProceedings() {
        publications = new HashSet<>();
        setId(idCounter.toString());
        idCounter++;
    }

    public String getNote() {
        return note;
    }

    public void setNote(String note) {
        this.note = note;
    }

    public Integer getNumber() {
        return number;
    }

    public void setNumber(Integer number) {
        this.number = number;
    }

    public Publisher getPublisher() {
        return publisher;
    }

    public void setPublisher(Publisher publisher) {
        this.publisher = publisher;
    }

    public String getVolume() {
        return volume;
    }

    public void setVolume(String volume) {
        this.volume = volume;
    }

    public String getIsbn() {
        return isbn;
    }

    public void setIsbn(String isbn) {
        if (this.getIsbn() != null) {
            if (this.getNote() == null) {
                this.setNote("");
            }
            this.setNote(this.getNote() + "\nISBN updated, old value was " + this.getIsbn());
        }
        this.isbn = isbn;
    }

    public Series getSeries() {
        return series;
    }

    public void setSeries(Series series) {
        this.series = series;
    }

    public ConferenceEdition getConferenceEdition() {
        return conferenceEdition;
    }

    public void setConferenceEdition(ConferenceEdition conferenceEdition) {
        this.conferenceEdition = conferenceEdition;
    }

    public Set<InProceedings> getPublications() {
        return publications;
    }

    public void setPublications(Set<InProceedings> publications) {
        this.publications = publications;
    }
}