package ch.ethz.globis.isk.persistence;

import ch.ethz.globis.isk.domain.JournalEdition;
import ch.ethz.globis.isk.domain.jpa.JpaJournalEdition;

import org.springframework.stereotype.Repository;

import com.db4o.query.Query;

import java.util.List;

@Repository
public class JpaJournalEditionDao extends JpaDao<String, JournalEdition> implements JournalEditionDao {

    @Override
    protected Class<JpaJournalEdition> getStoredClass() {
        return JpaJournalEdition.class;
    }

    @Override
    public JournalEdition createEntity() {
        return new JpaJournalEdition();
    }

    @Override
    public List<JournalEdition> findByJournalIdOrdered(String journalId) {
    	Query query = oc.query();
    	query.constrain(getStoredClass());
    	query.descend("journal").descend("id").constrain(journalId);
    	query.descend("year").orderAscending();
    	query.descend("volume").orderAscending();
    	query.descend("number").orderAscending();
    	return query.execute();
//        String findAuthorsQuery = "Select je from JournalEdition je JOIN je.journal j " +
//                "WHERE j.id = :journalId ORDER BY je.year, je.volume, je.number ASC";
//        Query query = em.createQuery(findAuthorsQuery);
//        query.setParameter("journalId", journalId);
//        return query.getResultList();
    }
}
