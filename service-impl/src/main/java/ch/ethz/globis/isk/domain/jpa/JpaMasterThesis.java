package ch.ethz.globis.isk.domain.jpa;

import ch.ethz.globis.isk.domain.MasterThesis;
import ch.ethz.globis.isk.domain.School;

public class JpaMasterThesis extends JpaPublication implements MasterThesis {
	private static Integer idCounter = 0;
	
    private School school;

    public JpaMasterThesis() { 
        setId(idCounter.toString());
        idCounter++;
    }

    public School getSchool() {
        return school;
    }

    public void setSchool(School school) {
        this.school = school;
    }
}