package ch.ethz.globis.isk.persistence;

import ch.ethz.globis.isk.domain.DomainObject;
import ch.ethz.globis.isk.util.Filter;
import ch.ethz.globis.isk.util.Operator;
import ch.ethz.globis.isk.util.Order;
import ch.ethz.globis.isk.util.OrderFilter;

import com.db4o.ObjectContainer;
import com.db4o.ObjectSet;
import com.db4o.query.Query;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Map.Entry;

public abstract class JpaDao<K extends Serializable, T extends DomainObject> implements Dao<K, T> {

    @Autowired
    @Qualifier("db4oObjectContainer")
    ObjectContainer oc;

    @Override
    public long countAllByFilter(Map<String, Filter> filterMap) {
        Query query = selectQueryFromFilterMap(filterMap, null);
        return query.execute().size();
    }

    @Override
    public long count() {
    	Query query = selectQueryFromFilterMap(null, null);
    	return query.execute().size();
    }

    @Override
    public Iterable<T> findAll() {
    	Query query = selectQueryFromFilterMap(null, null);
    	return query.execute();
    }

    @Override
    public T findOne(K id) {
    	Filter filter = new Filter(Operator.EQUAL, id);
    	Map<String, Filter> filterMap = new HashMap<String, Filter>();
    	filterMap.put("id", filter);

    	return findOneByFilter(filterMap);
    }

    @Override
    public T findOneByFilter(Map<String, Filter> filterMap) {
    	Query query = selectQueryFromFilterMap(filterMap, null);
    	ObjectSet<T> set = query.execute();
    	
    	// check if there is a result, if not: return null
    	if (set.size() > 0)
    		return set.get(0);
    	else
    		return null;
    	
//        CriteriaQuery<T> query = selectQueryFromFilterMap(filterMap, null);
//
//        try {
//            return em.createQuery(query).getSingleResult();
//        } catch (NoResultException nre) {
//            return null;
//        }
    }

    @Override
    public Iterable<T> findAllByFilter(Map<String, Filter> filterMap) {
    	Query query = selectQueryFromFilterMap(filterMap, null);
    	return query.execute();
    }

    @Override
    public Iterable<T> findAllByFilter(Map<String, Filter> filterMap, int start, int size) {
    	Query query = selectQueryFromFilterMap(filterMap, null);
    	ObjectSet<T> set = query.execute();
    	
    	return subList(set, start, size);
    }

    @Override
    public Iterable<T> findAllByFilter(Map<String, Filter> filterMap,
                                       List<OrderFilter> orderList,
                                       int start, int size) {
    	Query query = selectQueryFromFilterMap(filterMap, orderList);
    	ObjectSet<T> set = query.execute();
    	
    	return subList(set, start, size);
    }
    
    private Iterable<T> subList(ObjectSet<T> list, int start, int size){
    	int listSize = list.size();
    	ArrayList<T> array = new ArrayList<T>();
    	
    	for (int i=start; i<start+size; i++){
    		if (i<listSize){
    		T t = list.get(i);
    		array.add(t);
    		}
    		else{
    			break;
    		}
    	}
    	return array;
    	
//    	if(size > 0 && start+size >= listSize) return list.subList(start, listSize);
//    	else if(size > 0 && start+size < listSize) return list.subList(start, start+size);
//    	//else return new ArrayList<T>();
//    	else return null;
    }

    @Override
    public Iterable<T> findAllByFilter(Map<String, Filter> filterMap,
                                       List<OrderFilter> orderList) {
    	Query query = selectQueryFromFilterMap(filterMap, orderList);
    	return query.execute();
    }

    @Override
    public <S extends T> Iterable<S> insert(Iterable<S> entities) {
        for(S entity : entities) {
            insert(entity);
        }
        return entities;
    }

    
    @Override
    public <S extends T> S insert(S entity) {
        oc.store(entity);
        return entity;
    }

    protected abstract <S extends T> Class<S> getStoredClass();
       

//    protected Session getSession() {
//    	return oc.getClass(Session.class);
//    }

    protected List<T> queryByReferenceIdOrderByYear(String entity, String referenceName, String referenceId) {
    	Query query = oc.query();
    	try {
			query.constrain(Class.forName("ch.ethz.globis.isk.domain.jpa." + "Jpa" +entity));
			query.descend(referenceName).descend("id").constrain(referenceId);
			query.descend("year").orderAscending();
			return query.execute();
		} catch (ClassNotFoundException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
			return null;
		}
//        String idField = referenceName + "Id";
//        String findAuthorsQuery = "Select p from %s p JOIN p.%s e " +
//                "WHERE e.id = :%s ORDER BY p.year ASC";
//        findAuthorsQuery = String.format(findAuthorsQuery, entity, referenceName, idField);
//        Query query = em.createQuery(findAuthorsQuery);
//        query.setParameter(idField, referenceId);
//        return query.getResultList();
    }

  
    private Query selectQueryFromFilterMap(Map<String, Filter> filterMap,
    									  List<OrderFilter> orderList) {
    	Query query = oc.query();
    	query.constrain(getStoredClass());
    	
    	// add filter values
    	if (filterMap != null){
	    	for (Entry<String, Filter> entry : filterMap.entrySet()){
	    		String name = entry.getKey();
	    		Filter filter = entry.getValue();
	    		
	    		// case distinction over all operators
	    		Operator operator = filter.getOperator();
	    		
	    		if (Operator.EQUAL.equals(operator)){
	    			query.descend(name).constrain(filter.getValue());
	    		}
	    		else if (Operator.STRING_MATCH.equals(operator)){
	    			query.descend(name).constrain(filter.getValue().toString());
	    		}
	    	}
    	}
    	
    	//add order fields
    	if (orderList != null){
	    	for(OrderFilter filter : orderList){
	    		String name = filter.getField();
	    		Order order = filter.getOrder();
	    		
	    		// case distinction over all Order
	    		if (Order.DESC.equals(order)){
	    			query.descend(name).orderDescending();
	    		}
	    		else if (Order.ASC.equals(order)){
	    			query.descend(name).orderAscending();
	    		}
	    	}
    	}
    	
    	return query;
    }
}